module.exports = {
  assetsDir: 'assets',
  publicPath: process.env.NODE_ENV == 'production' ? '/' : '/',
  productionSourceMap:false,
  devServer: {
    proxy: {
      "/api": {
        "target": "http://scms.test.com/",
        "changeOrigin": true,
        "pathRewrite": {  }
      }
    }
  }
}